# DISEÑO ARQUITECTURAL DE SOFTWARE Y PATRONES

Repositorio de la materia **DISEÑO ARQUITECTURAL DE SOFTWARE Y PATRONES**
donde se cargaran los proyectos realizados en dicha asignatura 

# **Integrantes:**

- **Sebstian Lopez**        20142020013  
- **Ivan Arevalo**          20142020037    
- **Juan Sebastian Reyes**  20142020091  
- **Brayan Clavijo**        20142020135  

## [**DiagramasUML**](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/DiagramasUML)

Los **diagramas UML** nos perimiten ver un enfoque mas palpable de nuestro proyecto lo cual
nos permite ver posibles herrores que podemomos encontrar dentro de nuestras aplicaciones 
a la hora de iniciar la implementación de nuestro proyecto, en este repositorio se encuentran 
algunos ejemplos de **diagramas UML** aplicados a un sistema de notas. 

## [**Microservicios & Serverles**](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/Microservicios%20&%20Serveless)

### **Microservicios**

Una “arquitectura de microservicios” es un enfoque para desarrollar una aplicación software 
como una serie de pequeños servicios, cada uno ejecutándose de forma autónoma y comunicándose 
entre sí, consta un número mínimo de servicios que gestionan cosas comunes para los demás 
(como el acceso a base de datos), pero cada microservicio es pequeño y corresponde a un área 
de negocio de la aplicación.

### **Serverles**

Las arquitecturas sin servidor son arquitecturas de aplicaciones que incorporan servicios de terceros _"BaaS"_ y
que ejecutan codigo personalizado en una plataforma _"FaaS"_. Al usar estas y otras relacionadas como aplicaciones
de una pagina, dichas arquitecturas eliminan la necesidad de la idea de un servidor convencional sempre activo... [**ver mas**](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/Microservicios%20&%20Serveless/ReadMe.md)

## [**Sistema Academico**](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico)

**Archimete** es un lenguaje de modelado que permite una facil representacion de arquitecturas 
empresariales con el cual se busca una mejor vision de la empresa a la hora de desarrollar software 
sobres esta, la vision de la arquitectura dentro de **archimete** esta propuesta generalmene en tres capas: 
negosio, aplicación y tecnologia.  

En este repositorio se encuentran ejemplos de varios puntos de vistas basicos a la hora de
implementar una arquitectura de negosion, todos esptos aplicados a un sistema academico.

## Contenido

* [Primer Diseño](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Primermodelo)    
* [Capa de Negocio](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio)
	* [Modelo de Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/ModelodeActores)  
	* [Cooperacion Entre Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/CoperacionentreActores)  
	* [Modelo de Funciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeFunciones)
	* [Diagramas de Procesos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos)  
		* Consejerias  
		* Consulta de Horarios
		* Inscripcion de Materias
		* Evaluacion Docente
		* Grados  
		* Certificados  
		* Recibos de Pago
* [Capa De Aplicacion](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion)		  
	* [Dicagrama de cooperaciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/cooperacionAplicaciones)  
	* [Diagramas de Usos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos)  
		* Consejerias  
		* Consulta de Horarios
		* Inscripcion de Materias
		* Evaluacion Docente
		* Grados  
		* Certificados  
		* Recibos de Pago 
* [Capa de tecnologia](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Tecnologia)
	* [Usos Infraestructura](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Tecnologia)		