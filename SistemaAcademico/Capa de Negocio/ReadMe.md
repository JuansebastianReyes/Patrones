# [Capa de Negocio](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio)

## [Modelo de Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/ModelodeActores)
![actores](SistemaAcademico/Capa%20de%20Negocio/ModelodeActores/ModeloDeActores.png)

## [Cooperacion Entre Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/CoperacionentreActores)
![cooperacion](SistemaAcademico/Capa%20de%20Negocio/CoperacionentreActores/cooperacionEntreActores.png)

## [Modelo de funciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeFunciones)
![funciones](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeFunciones/ModeloFunciones.jpg)

## [Diagramas de Procesos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos)

### Proceso Consejeria
![consejeria](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consejerias.png)

### Proceso Consulta de Horarios
![horarios](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consulta%20Horarios.png)

### Proceso Inscripcion Materias
![inscrpcion](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/inscripcionProcesos.png)

### Proceso Evaluacion Docente
![evaluacionDocente](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/evaluacionDocente.png)

### Proceso Grados
![grados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/gradosProcesos.png)

### Proceso Certificados
![certificados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/certificadoProcesos.png)

### Proceso Recibos de Pago
![recibos](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/recibosProcesos.png)