# Proceso Consejeria
![consejeria](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consejerias.png)

# Proceso Consulta de Horarios
![horarios](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consulta%20Horarios.png)

# Proceso Inscripcion Materias
![inscrpcion](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/inscripcionProcesos.png)

# Proceso Evaluacion Docente
![evaluacionDocente](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/evaluacionDocente.png)

# Proceso Grados
![grados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/gradosProcesos.png)

# Proceso Certificados 
![certificados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/certificadoProcesos.png)

# Proceso Recibos de Pago
![recibos](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/recibosProcesos.png)
