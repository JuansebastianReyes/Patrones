# Sistema Academico

**Archimete** es un lenguaje de modelado que permite una facil representacion de arquitecturas 
empresariales con el cual se busca una mejor vision de la empresa a la hora de desarrollar software 
sobres esta, la vision de la arquitectura dentro de **archimete** esta propuesta generalmene en tres capas: 
negosio, aplicación y tecnologia.  

En este repositorio se encuentran ejemplos de varios puntos de vistas basicos a la hora de
implementar una arquitectura de negosion, todos esptos aplicados a un sistema academico.

## [Primer Versión](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Primermodelo)
![academico1](SistemaAcademico/Primermodelo/sistemaAcademico.png)

# [Capa de Negocio](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio)

## [Modelo de Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/ModelodeActores)
![actores](SistemaAcademico/Capa%20de%20Negocio/ModelodeActores/ModeloDeActores.png)

## [Cooperacion Entre Actores](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/CoperacionentreActores)
![cooperacion](SistemaAcademico/Capa%20de%20Negocio/CoperacionentreActores/cooperacionEntreActores.png)

## [Modelo de funciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeFunciones)
![funciones](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeFunciones/ModeloFunciones.jpg)

## [Diagramas de Procesos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos)

### Proceso Consejeria
![consejeria](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consejerias.png)

### Proceso Consulta de Horarios
![horarios](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/Consulta%20Horarios.png)

### Proceso Inscripcion Materias
![inscrpcion](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/inscripcionProcesos.png)

### Proceso Evaluacion Docente
![evaluacionDocente](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/evaluacionDocente.png)

### Proceso Grados
![grados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/gradosProcesos.png)

### Proceso Certificados
![certificados](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/certificadoProcesos.png)

### Proceso Recibos de Pago
![recibos](SistemaAcademico/Capa%20de%20Negocio/DiagramasdeProcesos/recibosProcesos.png)

# [Capa de Aplicacion](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion)

## [Dicagrama de cooperaciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/cooperacionAplicaciones)
![cooperaciones](SistemaAcademico/Capa%20de%20Aplicacion/cooperacionAplicaciones/cooperacionAplicaiones.png)

## [Diagramas de Usos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos)

### Certificados
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/certificados.png)

### Consejerias
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/consejerias.png)

### Evaluación Docente
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/evaluacion_docente.png)

### Grados
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/grados.png)

### Inscripcion De Materias
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/inscripcion_materias.png)

### Recibos
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/recibos.png)

# [Capa de tecnologia](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Tecnologia)

### [Usos Infraestructura](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Tecnologia/Usos%20Infraestructura)
![Infrestructuras](SistemaAcademico/Capa%20de%20Tecnologia/Usos%20Infraestructura/UsosInfraestucturas.png)
