# [Capa de Aplicacion](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion)

## [Dicagrama de cooperaciones](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/cooperacionAplicaciones)
![cooperaciones](SistemaAcademico/Capa%20de%20Aplicacion/cooperacionAplicaciones/cooperacionAplicaiones.png)

## [Diagramas de Usos](https://gitlab.com/JuansebastianReyes/Patrones/tree/master/SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos)

### Certificados
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/certificados.png)

### Consejerias
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/consejerias.png)

### Evaluación Docente
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/evaluacion_docente.png)

### Grados
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/grados.png)

### Inscripcion De Materias
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/inscripcion_materias.png)

### Recibos
![certificados](SistemaAcademico/Capa%20de%20Aplicacion/DiagramasdeUsos/recibos.png)