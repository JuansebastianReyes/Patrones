# **Microservicios**

Una “arquitectura de microservicios” es un enfoque para desarrollar una aplicación software 
como una serie de pequeños servicios, cada uno ejecutándose de forma autónoma y comunicándose 
entre sí, consta un número mínimo de servicios que gestionan cosas comunes para los demás 
(como el acceso a base de datos), pero cada microservicio es pequeño y corresponde a un área 
de negocio de la aplicación.  

Cada uno es independiente y su código debe poder ser desplegado sin afectar a los demás, donde
no hay reglas sobre qué tamaño tiene que tener cada microservicio, ni sobre cómo dividir la
aplicación en microservicios depende de la estructura pactada y que impacto tiene esta arquitectura
para mejorar el rendimiento, cuando hacemos una comparación sobre la forma monolítica de ver una
aplicación encontramos varias diferencias para saber en qué nos puede beneficiar una arquitectura
de este tipo de microservicios.  

Aplicación monolítica; En una aplicación monolítica toda la lógica se ejecuta en un único servidor
de aplicaciones. Las aplicaciones monolíticas típicas son grandes y construidas por múltiples equipos, 
requiriendo una orquestación cuidadosa del despliegue para cada cambio. También se consideran 
aplicaciones monolíticas cuando existen múltiples servicios de API que proporcionan la lógica de 
negocio, toda la capa de presentación es una sola aplicación web grande. En ambos casos, la arquitectura 
de microservicios puede proporcionar una alternativa.  

![imagen1](Microservicios%20&%20Serveless/microservicios1.png)  

Una de las ventajas de utilizar microservicios sobre una aplicación monolítica es la capacidad de 
publicar una aplicación grande como un conjunto de pequeñas aplicaciones (microservicios) que se pueden 
desarrollar, desplegar, escalar, manejar y visualizar de forma independiente. Los microservicios permiten 
a las empresas gestionar las aplicaciones de código base grande usando una metodología más práctica donde 
las mejoras incrementales son ejecutadas por pequeños equipos en bases de código y despliegues independientes.  

![imagen1](Microservicios%20&%20Serveless/microservicios2.png)  

Un buen ejemplo de la arquitectura de este tipo son los servicios web ya que son componentes de software 
ligeramente acoplados entregados a través de tecnologías estándar de Internet. Es decir, los servicios 
Web son aplicaciones de negocio auto descriptivas y modulares que exponen la lógica empresarial como servicios 
a través de Internet a través de la interfaz programable y donde el protocolo de Internet (IP) puede ser 
utilizado para encontrar e invocar esos servicios. Un servicio web es el elemento clave en la integración de 
diferentes sistemas de información, ya que los sistemas de información pueden basarse en diferentes plataformas, 
lenguajes de programación y tecnologías.

# **Serverless**

Las arquitecturas sin servidor son arquitecturas de aplicaciones que incorporan servicios de terceros _"BaaS"_ y
que ejecutan codigo personalizado en una plataforma _"FaaS"_. Al usar estas y otras relacionadas como aplicaciones
de una pagina, dichas arquitecturas eliminan la necesidad de la idea de un servidor convencional sempre activo.  

Se utilizó por primera vez para describir aplicaciones que incorporan de manera significativa o completa aplicaciones
y servicios alojados en la nube de terceros para administrar la lógica y el estado del servidor. Suelen ser aplicaciones
de "cliente rico", como aplicaciones web de una sola página o aplicaciones móviles que utilizan el vasto ecosistema 
de bases de datos accesibles en la nube (por ejemplo, Parse, Firebase), servicios de autenticación (por ejemplo, 
Auth0, AWS Cognito) y pronto. Estos tipos de servicios se describieron anteriormente como " backend (móvil) como servicio ", 
y utilizo _"BaaS"_ como taquigrafía en el resto de este artículo.  

Sin servidor también puede significar aplicaciones donde la lógica del lado del servidor todavía está escrita por el 
desarrollador de aplicaciones, pero a diferencia de las arquitecturas tradicionales, se ejecuta en contenedores de cómputo 
sin estado que son activados por eventos, efímeros (puede durar solo una invocación) y totalmente administrados por un 
tercero. Una forma de pensar en esto es "Funciones como servicio" o "FaaS" . (Nota: la fuente original para este nombre, 
un tweet de @ marak, ya no está disponible públicamente). AWS Lambda es una de las implementaciones más populares de una 
plataforma de Funciones como Servicio actualmente, pero hay muchas otras. , también.  

![imagen1](Microservicios%20&%20Serveless/serverless.png)

